import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
Vue.use(VueRouter)


// //获取原型对象上的push函数
// const originalPush = VueRouter.prototype.push
// //修改原型对象中的push方法
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => err)
// }


const routes = [
 
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue'),
  },
  {
    path: '/',
    name: 'Home',
    redirect: {name : 'Users'},
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    children:[
      {
        path: 'users',
        name: 'Users',
        component: () => import(/* webpackChunkName: "users" */ '../views/users/Users.vue'),
      },
      {
        path: 'rights',
        name: 'Right',
        component: () => import(/* webpackChunkName: "rights" */ '../views/rights/index.vue'),
      },
      {
        path: 'roles',
        name: 'Roles',
        component: () => import(/* webpackChunkName: "roles" */ '../views/rights/Roles.vue'),
      },
      {
        path: 'reports',
        name: 'Reports',
        component: () => import(/* webpackChunkName: "reports" */ '../views/reports/index.vue'),
      },
      {
        path: 'orders',
        name: 'Orders',
        component: () => import(/* webpackChunkName: "orders" */ '../views/orders/index.vue'),
      },
      {
        path: 'goods',
        name: 'Goods',
        component: () => import(/* webpackChunkName: "goods" */ '../views/goods/index.vue'),
      },
      {
        path: 'params',
        name: 'Params',
        component: () => import(/* webpackChunkName: "users" */ '../views/goods/Params.vue'),
      },
      {
        path: 'categories',
        name: 'Categories',
        component: () => import(/* webpackChunkName: "users" */ '../views/goods/Categories.vue'),
      },
      {
        path: 'addgoods',
        name: 'AddGoods',
        component: () => import(/* webpackChunkName: "addgoods" */ '../views/goods/addGoods.vue'),
       },
    ]
  }
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]


const router = new VueRouter({
  routes
})


router.beforeEach((to,from,next) =>{
  if(to.name === 'Login'){
    next()
  }else {
    if(store.state.token){
      next()
    }else{
      next({
        name:'Login'
      })
    }
  }
})


export default router