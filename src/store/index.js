import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username : localStorage.getItem('username') || '',
    token: localStorage.getItem('token') || ''
  },
  mutations: {
    SET_USERNAME(state,username){
      state.username = username
      localStorage.setItem('username',username)
    },
    SET_TOKEN(state,token){
      state.token = token
      localStorage.setItem('token',token)
    }
  },
  actions: {
  },
  modules: {
  }
})
